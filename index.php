<?php
	session_start();
	if($_SESSION["user_id"] !== null){
		header("Location: member.php");
		exit();
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="medsos.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:200,400,600">
<link rel="shortcut icon" type="image/png" href="favicon.png">
<title>IvyCommunity</title>
</head>
<body class="nomargin">
<div class="navtop">
<a href="index.php"><img class="logo" src="favicon.png"></a>
</div>
<div class="container">
<div class="row"><h3>Login</h3></div>
<div class="row">
<div class="error">
<?php
	if(isset($_COOKIE["lerror"])){
		echo "Username/Password Incorrect";
		setcookie("lerror", "", time()-1);
	}
?>
</div>
</div>
<div class="row">
<form action="login.php" method="post">
	<input type="email" name="email" placeholder="E-Mail">
	<input type="password" name="pass" placeholder="Password">
	<input type="submit" class="btn" value="Login">
</form>
</div>
<div class="row line"></div>
<div class="row">Doesn't have an account? Sign up</div>
<div class="row separate"></div>
<div class="row">
<form action="register.php" method="post" >
  <input name="username" type="text" id="username" placeholder="Username"/>
  <input name="email" type="email" id="email" placeholder="E-Mail"/>
	<input name="password" type="password" id="password" placeholder="Password"/>
	<br>
	<input name="submit" type="submit" class="btn" value="Register"/>
</form>
</div>
</div>
</body>
</html>
