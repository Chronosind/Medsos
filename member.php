<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="medsos.css">
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,600">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Member page</title>
	</head>
	<body class="nomargin">
		<div class="navtop">
			<?php session_start(); ?>
			<a href="index.php" class="inline"><img src="favicon.png" class="logo"></img></a>
			<div class="inline pull right">
				<a href="profile.php?id=<?php echo $_SESSION["user_id"]; ?>" class="inline profile self" style="text-decoration: none;"><img class="profile small" src="profpic/<?php 
					include("configdb.php");
					$db = connect("medsos", "MuA631212", "medsos");
					$res = $db->query("SELECT profpic FROM user WHERE id=".$_SESSION["user_id"]);
					if($res->num_rows > 0){
						echo $res->fetch_assoc()["profpic"];
						if($res->fetch_assoc()["profpic"] == null){
							echo "default.png";
						}
					}
					$db->close();
				?>"></img><span class="profile name"><?php echo $_SESSION["user_name"]; ?></span></a>
				<div class="inline" style="position: relative; float: right;"><a href="logout.php" class="btn inline">Logout</a></div>
				</div>
		</div>
		<div class="container">
			<?php
			 session_start();
			 if($_SESSION['user_id'] == '')
			 {
				header("Location: index.php");
				exit;
			 }
			 echo "Hi ".$_SESSION['user_name']."!";
			?>
		</div>
	</body>
</html>
